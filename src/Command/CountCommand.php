<?php
namespace App\Command;

use App\Service\CalcService;
use App\Service\SearchService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CountCommand extends Command
{
    protected static $defaultName = 'app:count';
    /**
     * @var SearchService
     */
    private $searchService;
    /**
     * @var CalcService
     */
    private $calcService;

    /**
     * CountCommand constructor.
     * @param SearchService $searchService
     * @param CalcService $calcService
     * @param string|null $name
     */
    public function __construct(SearchService $searchService, CalcService $calcService, string $name = null)
    {
        parent::__construct($name);
        $this->searchService = $searchService;
        $this->calcService = $calcService;
    }

    protected function configure()
    {
        parent::configure();
        $this->addArgument('path');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $files = $this->searchService->scan($input->getArgument('path'), '/count/');
        $summ = $this->calcService->calculate($files);
        $output->writeln($summ);
        return Command::SUCCESS;
    }
}