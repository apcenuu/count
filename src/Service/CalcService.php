<?php


namespace App\Service;


class CalcService
{
    /**
     * @param $files
     * @return int
     */
    public function calculate(array $files)
    {
        $summ = 0;
        foreach ($files as $file) {
            $content = file_get_contents($file);
            $numbers = explode("\n", $content);
            foreach ($numbers as $number) {
                $summ += $number;
            }
        }
        return $summ;
    }
}