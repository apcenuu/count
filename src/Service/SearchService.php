<?php


namespace App\Service;


use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;

class SearchService
{
    /**
     * @param string $folder
     * @param string $pattern
     * @return array
     */
    public function scan(string $folder, string $pattern)
    {
        $directory = new RecursiveDirectoryIterator($folder);
        $iterator = new RecursiveIteratorIterator($directory);
        $files = new RegexIterator($iterator, $pattern, RegexIterator::GET_MATCH);
        $fileList = [];
        foreach($files as $file) {
            $fileList[] = $iterator->current()->getPathName();
        }
        return $fileList;
    }
}